import cv2
import numpy as np

#Inicializamos los paramtros deldetector de arucos
parametros = cv2.aruco.DetectorParameters_create()

# Load the predefined dictionary
dictionary = cv2.aruco.Dictionary_get(cv2.aruco.DICT_5X5_100)


cap = cv2.VideoCapture(0)
cap.set(3,1280)
cap.set(4,720)

while True:
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #Detectamos los marcadores en la imagen
    esquinas, ids, candidatos_malos = cv2.aruco.detectMarkers(gray, dictionary, parameters = parametros)

    if np.all(ids != None):
        aruco = cv2.aruco.drawDetectedMarkers(frame, esquinas)

        #Extramos los puntos de las esquinas en coordenadas separadas
        c1 = (esquinas[0][0][0][0],esquinas[0][0][0][1])
        c2 = (esquinas[0][0][1][0],esquinas[0][0][1][1])
        c3 = (esquinas[0][0][2][0], esquinas[0][0][2][1])
        c4 = (esquinas[0][0][3][0], esquinas[0][0][3][1])

        copy = frame
        #Leemos la imagen que vamos a sobreponer
        imagen = cv2.imread("halo.jpg")
        #Extramos el tamaño de la iamgen
        tamaño = imagen.shape
        #Organizamos las coordenadas del aruco en una matriz
        puntos_aruco = np.array([c1,c2,c3,c4])

        #Organizamos las coordenadas de la imagen en otra matriz
        puntos_imagen = np.array([
            [0,0],
            [tamaño[1] - 1,0],
            [tamaño[1]-1, tamaño[0]-1],
            [0,tamaño[0]-1]
        ],dtype = float)

        #Realizamos la superposición de la imagen (Homografía)
        h, estado = cv2.findHomography(puntos_imagen, puntos_aruco)

        #Realizamos la transformación de prespectiva
        perspectiva = cv2.warpPerspective(imagen,h,(copy.shape[1],copy.shape[0]))
        cv2.fillConvexPoly(copy, puntos_aruco.astype(int),0,16)
        copy = copy + perspectiva
        cv2.imshow("Realidad Virtual",copy)
    else:
        cv2.imshow("Realidad Virtual",frame)

    k = cv2.waitKey(1)
    if k == 27:
        break
cap.release()
cv2.destroyAllWindows()




# See PyCharm help at https://www.jetbrains.com/help/pycharm/
